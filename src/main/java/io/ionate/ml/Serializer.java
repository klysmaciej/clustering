package io.ionate.ml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializer {

    private static final Logger LOG = LoggerFactory.getLogger(Serializer.class.getName());

    public static boolean serialize(Object obj, String path) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static Object deserialize(byte[] bytes) throws ClassNotFoundException {
        Object obj = null;
        try {
            ByteArrayInputStream bytesStream = new ByteArrayInputStream(bytes);
            ObjectInputStream in = new ObjectInputStream(bytesStream);
            obj = in.readObject();
            in.close();
            bytesStream.close();
        } catch (IOException ex) {
            LOG.warn(ex.getMessage());
        }
        return obj;
    }
}
