package io.ionate.ml;

import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.EagerDataSet;
import io.ionate.ml.data.source.DataSource;
import io.ionate.ml.data.source.es.EsDataSource;
import io.ionate.ml.es.EsAccessToken;
import io.ionate.ml.es.EsClient;
import io.ionate.ml.model.StringModel;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.clustering.GMeans;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GroupingProgram {

    private static final Logger LOG = LoggerFactory.getLogger(GroupingProgram.class);

    public static void main(String[] args) {
        LOG.info("Building ES Access Token provider");
        Supplier<String> accessToken = new EsAccessToken();
        LOG.info("Building ES client");
        Supplier<Client> client = new EsClient(accessToken);
        LOG.info("Building ES data source");
        DataSource<SearchHit> esDataSource = new EsDataSource(client, accessToken);

        LOG.info("Building data set");
        DataSet<StringModel> dataSet = new EagerDataSet<>(esDataSource, new StringModel.EsMapper());
        List<StringModel> test = dataSet.stream()
                .filter(model ->
                        model.theString().contains("ProductController") || model.theString().contains("CartController"))
                .collect(Collectors.toList());

        LOG.info("Building features array");
//        double[][] features = new double[test.size()][StringFeaturesExtractor.DIM];
//        dataSet.copyToArrayOfVectors(features);
        double[][] features = test.stream().map(StringModel::toVector).toArray(double[][]::new);

        LOG.info("Performing clustering");
        GMeans means = new GMeans(features, 10);
        LOG.info(means.toString());

        SimpleStatistics.print(means, dataSet);
    }

    private static Predicate<StringModel> subset() {
        return model -> model.theString().contains("Creating") || model.theString().contains("Deleting");
    }
}
