package io.ionate.ml.clustering;

import io.ionate.ml.GroupingProgram;
import io.ionate.ml.clustering.persistence.ClusterLoader;
import io.ionate.ml.clustering.persistence.ClusterRepository;
import io.ionate.ml.clustering.persistence.DenseWordsLoader;
import io.ionate.ml.clustering.persistence.PostgresDataSource;
import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.EagerDataSet;
import io.ionate.ml.data.source.DataSource;
import io.ionate.ml.data.source.es.EsDataSource;
import io.ionate.ml.es.EsAccessToken;
import io.ionate.ml.es.EsClient;
import io.ionate.ml.model.StringModel;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.function.Supplier;

public class Clustering {

    private static final Logger LOG = LoggerFactory.getLogger(GroupingProgram.class);

    private final javax.sql.DataSource dataSource = PostgresDataSource.create();
    private final String containerName = "log-generator";
    private final String logFilename = "log-generator.log";

    private void run() {
        LOG.info("Building ES Access Token provider");
        Supplier<String> accessToken = new EsAccessToken();
        LOG.info("Building ES client");
        Supplier<Client> client = new EsClient(accessToken);
        LOG.info("Building ES data source");
        DataSource<SearchHit> esDataSource = new EsDataSource(client, accessToken);

        LOG.info("Building data set");
        DataSet<StringModel> dataSet = new EagerDataSet<>(esDataSource, new StringModel.EsMapper());

        performLearning(dataSet);
        test(dataSet.iterator().next());
    }

    private void performLearning(DataSet<StringModel> dataSet) {
        WordSummary summary = new WordSummary();
        dataSet.stream()
                .flatMap(model -> Arrays.stream(model.theString().split(" ")))
                .filter(word -> !word.isEmpty())
                .filter(word -> word.toLowerCase().chars().filter(isLowerCaseLetter()).count() == word.length())
                .forEach(summary::add);

        DenseWords denseWords = summary.getMostFrequentWords(100);
        ClusterCandidates clusterCandidates = new ClusterCandidates();
        dataSet.stream()
                .map(model -> model.theString().split(" "))
                .map(denseWords::filterOutNonDenseWords)
                .forEach(clusterCandidates::add);

        Clusters clusters = clusterCandidates.getMostFrequentClusters(100);

        ClusterRepository repository = new ClusterRepository(dataSource);
        repository.save(containerName, logFilename, clusters, denseWords);
    }

    private static IntPredicate isLowerCaseLetter() {
        return letter -> letter >= 95 && letter <= 122;
    }

    private void test(StringModel model) {
        ClusterLoader clusterLoader = new ClusterLoader(dataSource);
        DenseWordsLoader denseWordsLoader = new DenseWordsLoader(dataSource);
        DenseWords loadedDenseWords = denseWordsLoader.load(containerName, logFilename);
        Clusters loadedClusters = clusterLoader.load(containerName, logFilename);

        String[] words = Arrays.stream(model.theString().split(" ")).filter(word -> !word.isEmpty()).toArray(String[]::new);
        ClusterKey clusterKey = loadedDenseWords.filterOutNonDenseWords(words);
        LOG.info("Log: {}", model.theString());
        LOG.info("ClusterKey: {}", loadedClusters
                .predict(clusterKey)
                .map(Clusters.Cluster::getKey)
                .map(ClusterKey::toString)
                .orElse("Cluster not found")
        );
    }

    public static void main(String[] args) {
        new Clustering().run();
    }
}
