package io.ionate.ml;

import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.model.StringModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.clustering.PartitionClustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

class SimpleStatistics {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleStatistics.class);

    static void print(PartitionClustering<double[]> clustering, DataSet<StringModel> dataSet) {
        Map<Integer, List<StringModel>> clustersWithModels = new HashMap<>();
        for(int i = 0; i < clustering.getNumClusters(); ++i) {
            clustersWithModels.put(i, new ArrayList<>());
        }

        for(StringModel model : dataSet) {
            int clusterId = clustering.predict(model.toVector());
            List<StringModel> modelsInCluster = clustersWithModels.get(clusterId);
            modelsInCluster.add(model);
        }

        double averageScore = 0;
        int emptyClusters = 0;
        for(Map.Entry<Integer, List<StringModel>> cluster : clustersWithModels.entrySet()) {
            List<StringModel> models = cluster.getValue();
            long createProductCount = models.stream().filter(createProduct()).count();
            long updateProductCount = models.stream().filter(updateProduct()).count();
            long deleteProductCount = models.stream().filter(deleteProduct()).count();
            long addToCartCount = models.stream().filter(addToCart()).count();
            long removeFromCartCount = models.stream().filter(removeFromCart()).count();

            LOG.info("<----- Statistics of Cluster {} ----->", cluster.getKey());
            if(models.isEmpty()) {
                LOG.info("CLUSTER IS EMPTY !!!");
                ++emptyClusters;
                continue;
            }
            LOG.info("Create product: {}", createProductCount);
            LOG.info("Update product: {}", updateProductCount);
            LOG.info("Delete product: {}", deleteProductCount);
            LOG.info("Add to cart: {}", addToCartCount);
            LOG.info("Remove from cart: {}", removeFromCartCount);

            long maxCount = Math.max(
                    Math.max(
                            Math.max(createProductCount, updateProductCount),
                            Math.max(deleteProductCount, addToCartCount)),
                    removeFromCartCount);

            double score = 100.0 * maxCount/models.size();
            averageScore += score;
            LOG.info("Score: {} %", score);
        }

        LOG.info("<----- SUMMARY ----->");
        LOG.info("Data set size: {}", dataSet.size());
        LOG.info("Empty clusters: {}", emptyClusters);
        LOG.info("Number of clusters found: {}, actual number of clusters: {}", clustering.getNumClusters(), 5);
        LOG.info("AVERAGE SCORE: {}%", averageScore/clustering.getNumClusters());
    }

    private static Predicate<StringModel> createProduct() {
        return (model) -> model.theString().contains("Creating new product");
    }

    private static Predicate<StringModel> updateProduct() {
        return (model) -> model.theString().contains("Updating product");
    }

    private static Predicate<StringModel>  deleteProduct() {
        return (model) -> model.theString().contains("Deleting product");
    }

    private static Predicate<StringModel> addToCart() {
        return (model) -> model.theString().contains("Adding product");
    }

    private static Predicate<StringModel> removeFromCart() {
        return (model) -> model.theString().contains("Removing product");
    }
}
