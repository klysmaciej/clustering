package io.ionate.ml.ui.menu;

import io.ionate.ml.Serializer;
import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.data.source.PeriodicSource;
import io.ionate.ml.neural.NeuralServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Menus {

    private static final Logger LOG = LoggerFactory.getLogger(Menus.class);
    private final Scanner scanner = new Scanner(System.in);
    private final NeuralServer server;

    public Menus(NeuralServer server) {
        this.server = server;
    }

    public void optionOne() {
        List<PeriodicSource> dataSources = server.PERIODIC_SOURCES.getAll();
        IntStream.range(1, dataSources.size() + 1)
                .mapToObj(index -> index + ". " + dataSources.get(index - 1).name())
                .forEach(LOG::info);
    }

    public void optionTwo() {
        List<DataSet<DoubleBucket>> dataSets = server.RAW_DATA_SETS.getAll();
        IntStream.range(1, dataSets.size() + 1)
                .mapToObj(index -> String.format("%d. %s %s{size=%d}",
                        index,
                        dataSets.get(index - 1).name(),
                        dataSets.get(index - 1).guid(),
                        dataSets.get(index - 1).size()))
                .forEach(LOG::info);
    }

    public void optionThree() {
        List<DataSet<Bucket>> dataSets = server.AGGREGATED_DATA_SETS.getAll();
        IntStream.range(1, dataSets.size() + 1)
                .mapToObj(index -> String.format("%d. %s %s{size=%d}",
                        index,
                        dataSets.get(index - 1).name(),
                        dataSets.get(index - 1).guid(),
                        dataSets.get(index - 1).size()))
                .forEach(LOG::info);
    }

    public NeuralNetwork optionFour() {
        LOG.info("Enter filename: ");
        try {
            return (NeuralNetwork) Serializer
                    .deserialize(Files.readAllBytes(Paths.get(scanner.next() + ".nn")));
        } catch (ClassNotFoundException | IOException ex) {
            LOG.warn(ex.getMessage());
            return null;
        }
    }
}
