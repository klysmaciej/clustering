package io.ionate.ml.ui.menu;

import io.ionate.ml.Serializer;
import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.detection.DetectionEngine;
import io.ionate.ml.detection.DetectionResult;
import io.ionate.ml.detection.DetectionSupplier;
import io.ionate.ml.neural.NeuralServer;
import io.ionate.ml.neural.TrainingEngine;
import io.ionate.ml.neural.training.NeuralSettings;
import io.ionate.ml.neural.training.ProbabilityTrainingSettings;
import io.ionate.ml.ui.TrainingDemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.util.Objects;
import java.util.Scanner;

public class TrainMenu {

    private static final Logger LOG = LoggerFactory.getLogger(TrainMenu.class);
    private final Scanner scanner = new Scanner(System.in);
    private final NeuralServer server;

    private final TrainingEngine trainingEngine = new TrainingEngine();
    private final DetectionEngine detection = new DetectionEngine();

    private final NeuralSettings fixedNeuralSettings = new NeuralSettings.FixedSettings();
    private NeuralSettings customNeuralSettings;

    public TrainMenu(NeuralServer server) {
        this.server = server;
    }

    public NeuralNetwork run() {
        TrainingEngine.TrainingResult trainingResult = this.performTraining();

        Bucket[] trainingBuckets = trainingResult.getSettings().trainingSet().getBuckets();
        DoubleBucket[] realSignal = server.RAW_DATA_SETS.getAll().get(1).toArray(new DoubleBucket[0]);
        Bucket[] realSignalBucketed = server.AGGREGATED_DATA_SETS.getAll().get(1).toArray(new Bucket[0]);

        NeuralNetwork nn = trainingResult.getNn();
        DetectionResult detectionResult = detection
                .detect(nn, realSignalBucketed, new DetectionSupplier.ProbabilityDetecion());

        this.showResults(trainingBuckets, realSignal, realSignalBucketed, detectionResult);
        this.maybeSaveNeural(nn);
        return nn;
    }

    private TrainingEngine.TrainingResult performTraining() {
        final NeuralSettings neuralSettings = this.chooseNeuralSettings();
        ProbabilityTrainingSettings trainingSettings = new ProbabilityTrainingSettings(
                server.AGGREGATED_DATA_SETS.getAll().get(0),
                neuralSettings
        );

        TrainingEngine.TrainingResult trainingResult = trainingEngine.doTraining(trainingSettings);
        LOG.info("Waiting for training result...");
        return trainingResult;
    }

    private NeuralSettings chooseNeuralSettings() {
        LOG.info("Which settings do you want to use ?");
        LOG.info("1. Fixed: {}", fixedNeuralSettings);
        LOG.info("2. Custom settings.");
        if (Objects.nonNull(customNeuralSettings)) {
            LOG.info("3. Previously used custom settings: {}", customNeuralSettings);
        }
        int option = scanner.nextInt();
        if (option == 1) {
            return fixedNeuralSettings;
        } else if (option == 2) {
            customNeuralSettings = createCustomSettings();
            return customNeuralSettings;
        } else if (Objects.nonNull(customNeuralSettings) && option == 3) {
            return customNeuralSettings;
        }
        return fixedNeuralSettings;
    }

    private NeuralSettings createCustomSettings() {
        LOG.info("Learning rate: ");
        double learningRate = scanner.nextDouble();
        LOG.info("Number of hidden layers: ");
        int layersCount = scanner.nextInt();
        int[] layersSize = new int[layersCount];
        for (int i = 0; i < layersCount; ++i) {
            LOG.info("Layer {} size: ", i);
            layersSize[i] = scanner.nextInt();
        }
        LOG.info("Number of epochs: ");
        long epochs = scanner.nextLong();
        LOG.info("Stop value: ");
        double stopValue = scanner.nextDouble();

        return new NeuralSettings.CustomSettings(learningRate, layersSize, epochs, stopValue);
    }

    private void showResults(Bucket[] trainingBuckets,
                             DoubleBucket[] realSignal,
                             Bucket[] realSignalBucketed,
                             DetectionResult detectionResult) {
        final TrainingDemo demo = new TrainingDemo();
        demo.updateTrainingDemo(trainingBuckets, realSignal, realSignalBucketed, detectionResult);
        demo.setVisible(true);
    }

    private void maybeSaveNeural(NeuralNetwork nn) {
        LOG.info("Do you want to save ? (y/n)");
        String answer = scanner.next();
        if (answer.equalsIgnoreCase("y")) {
            LOG.info("Enter filename: ");
            Serializer.serialize(nn, scanner.next() + ".nn");
        }
    }
}
