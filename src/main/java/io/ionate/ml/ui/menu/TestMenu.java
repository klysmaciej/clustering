package io.ionate.ml.ui.menu;

import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.detection.DetectionEngine;
import io.ionate.ml.detection.DetectionResult;
import io.ionate.ml.detection.DetectionSupplier;
import io.ionate.ml.neural.NeuralServer;
import io.ionate.ml.ui.TrainingDemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

public class TestMenu {

    private static final Logger LOG = LoggerFactory.getLogger(TrainMenu.class);
    private final NeuralServer server;

    public TestMenu(NeuralServer server) {
        this.server = server;
    }

    public void run(NeuralNetwork nn) {
        DoubleBucket[] realSignal = server.RAW_DATA_SETS.getAll().get(1).toArray(new DoubleBucket[0]);
        Bucket[] realSignalBucketed = server.AGGREGATED_DATA_SETS.getAll().get(1).toArray(new Bucket[0]);

        DetectionEngine detection = new DetectionEngine();
        DetectionResult detectionResult = detection
                .detect(nn, realSignalBucketed, new DetectionSupplier.ProbabilityDetecion());

        final TrainingDemo demo = new TrainingDemo();
        demo.updateTrainingDemo(realSignal, realSignalBucketed, detectionResult);
        demo.setVisible(true);
    }
}
