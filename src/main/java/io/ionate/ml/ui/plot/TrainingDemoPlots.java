package io.ionate.ml.ui.plot;

import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.detection.DetectionResult;
import smile.plot.Line;
import smile.plot.LinePlot;
import smile.plot.PlotCanvas;

import java.awt.Color;
import java.util.Arrays;
import java.util.stream.IntStream;

import static io.ionate.ml.data.ploting.PlotUtils.prepareDataToPlotLine;

public class TrainingDemoPlots {
    public static PlotCanvas trainingSetPlot(Bucket[] buckets) {
        double[] xAxis = IntStream.range(0, buckets.length).mapToDouble(v -> v).toArray();
        double[] means = Arrays.stream(buckets).map(Bucket::getMean).mapToDouble(Double::doubleValue).toArray();
        double[] mins = Arrays.stream(buckets).map(Bucket::getMin).mapToDouble(Double::doubleValue).toArray();
        double[] maxs = Arrays.stream(buckets).map(Bucket::getMax).mapToDouble(Double::doubleValue).toArray();

        PlotCanvas sourceSignal = LinePlot.plot("Means", prepareDataToPlotLine(xAxis, means), Line.Style.SOLID, Color.BLUE);
        sourceSignal.line("Max", prepareDataToPlotLine(xAxis, maxs), Line.Style.SOLID, Color.RED);
        sourceSignal.line("Min", prepareDataToPlotLine(xAxis, mins), Line.Style.SOLID, Color.GREEN);
        sourceSignal.setTitle("Source signal (training signal)");

        return sourceSignal;
    }

    public static PlotCanvas realSignalPlot(DoubleBucket[] elements) {
        double[] sourcePeaks = Arrays.stream(elements).mapToDouble(DoubleBucket::getValue).toArray();
        double[] sourcePeaksArgs = IntStream.range(0, sourcePeaks.length).mapToDouble(v -> v).toArray();

        PlotCanvas realSignal = LinePlot.plot("'Real' signal with peaks",
                prepareDataToPlotLine(sourcePeaksArgs, sourcePeaks), Line.Style.SOLID, Color.BLUE);
        realSignal.setTitle("'Real' signal with peaks");

        return realSignal;
    }

    public static PlotCanvas realSignalBucketedPlot(Bucket[] buckets) {
        double[] plotArgs = IntStream.range(0, buckets.length).mapToDouble(v -> v).toArray();

        PlotCanvas bucketedRealSignal = LinePlot.plot("Aggregated 'real' signal with peaks",
                prepareDataToPlotLine(plotArgs, Arrays.stream(buckets).mapToDouble(Bucket::getMean).toArray()), Line.Style.SOLID, Color.BLUE);
        bucketedRealSignal.setTitle("Aggregated 'real' signal with peaks");

        return bucketedRealSignal;
    }

    public static PlotCanvas potentialAnomaliesPlot(Bucket[] buckets, DetectionResult detectionResult) {
        double[] plotArgs = IntStream.range(0, buckets.length).mapToDouble(v -> v).toArray();

        PlotCanvas potentialAnomalies = LinePlot.plot("Signal",
                prepareDataToPlotLine(plotArgs, Arrays.stream(buckets).mapToDouble(Bucket::getMean).toArray()), Line.Style.SOLID, Color.BLACK);
        potentialAnomalies.points(prepareDataToPlotLine(detectionResult.getArgs(), detectionResult.getValues()), Color.RED);
        potentialAnomalies.setTitle("Potential anomalies");

        return potentialAnomalies;
    }
}
