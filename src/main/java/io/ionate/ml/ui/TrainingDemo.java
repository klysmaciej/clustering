package io.ionate.ml.ui;

import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.detection.DetectionResult;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import static io.ionate.ml.ui.plot.TrainingDemoPlots.potentialAnomaliesPlot;
import static io.ionate.ml.ui.plot.TrainingDemoPlots.realSignalBucketedPlot;
import static io.ionate.ml.ui.plot.TrainingDemoPlots.realSignalPlot;
import static io.ionate.ml.ui.plot.TrainingDemoPlots.trainingSetPlot;

public class TrainingDemo extends JFrame {

    private final int WIDTH = 1680;
    private final int HEIGHT = 1050;
    private final int PLOT_COUNT = 4;
    private final GridLayout layout = new GridLayout(PLOT_COUNT, 1);
    private final JPanel plotPanel = new JPanel(layout);

    public TrainingDemo() throws HeadlessException {
        this.setTitle("Training DEMO");
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setLocationRelativeTo(null);
        this.getContentPane().add(plotPanel);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }

    public void updateTrainingDemo(Bucket[] trainingBuckets,
                                   DoubleBucket[] realSignal,
                                   Bucket[] realSignalBucketed,
                                   DetectionResult detectionResult) {
        this.layout.setRows(4);
        plotPanel.add(trainingSetPlot(trainingBuckets), 0);
        plotPanel.add(realSignalPlot(realSignal), 1);
        plotPanel.add(realSignalBucketedPlot(realSignalBucketed), 2);
        plotPanel.add(potentialAnomaliesPlot(realSignalBucketed, detectionResult), 3);
    }

    public void updateTrainingDemo(DoubleBucket[] realSignal,
                                   Bucket[] realSignalBucketed,
                                   DetectionResult detectionResult) {
        this.layout.setRows(3);
        plotPanel.add(realSignalPlot(realSignal), 0);
        plotPanel.add(realSignalBucketedPlot(realSignalBucketed), 1);
        plotPanel.add(potentialAnomaliesPlot(realSignalBucketed, detectionResult), 2);
    }
}
