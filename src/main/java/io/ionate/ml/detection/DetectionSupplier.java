package io.ionate.ml.detection;

import io.ionate.ml.data.set.Bucket;

public interface DetectionSupplier {
    double[] inputVector(Bucket bucket);
    double[] outputVector();
    double detectionValue(Bucket bucket, double[] output);
    boolean isHit();

    class ProbabilityDetecion implements DetectionSupplier {
        private final double[] inputVector = new double[2];
        private final double[] outputVector = new double[1];

        @Override
        public double[] inputVector(Bucket bucket) {
            inputVector[0] = bucket.getMean();
            inputVector[1] = bucket.getStdDev();
            return inputVector;
        }

        @Override
        public double[] outputVector() {
            return outputVector;
        }

        @Override
        public double detectionValue(Bucket bucket, double[] output) {
            return bucket.getMean();
        }

        @Override
        public boolean isHit() {
            return outputVector[0] < 0.25;
        }
    }
}
