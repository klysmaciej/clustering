package io.ionate.ml.detection;

import io.ionate.ml.neural.TrainingEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstantTest {

    private static final Logger LOG = LoggerFactory.getLogger(InstantTest.class);
    private static double ANOMALY_PROBABILITY_THRESHOLD = 0.9;

    private final TrainingEngine trainingEngine;

    public InstantTest(TrainingEngine trainingEngine) {
        this.trainingEngine = trainingEngine;
    }

    public void doTest(double valueToTest) {
//        double[] predictedProbability = trainingEngine.predict(new double[] {valueToTest} );
//        String verdict = predictedProbability[0] > ANOMALY_PROBABILITY_THRESHOLD ?
//                "OK" : "WRONG";
//        LOG.info("Value={}, probability={}{{}}", valueToTest, verdict, predictedProbability[0]);
    }
}
