package io.ionate.ml.detection;

public class DetectionResult {

    private final double[] args;
    private final double[] values;

    public DetectionResult(double[] args, double[] values) {
        this.args = args;
        this.values = values;
    }

    public double[] getArgs() {
        return args;
    }

    public double[] getValues() {
        return values;
    }
}
