package io.ionate.ml.detection;

import io.ionate.ml.data.set.Bucket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DetectionEngine {

    private static final Logger LOG = LoggerFactory.getLogger(DetectionEngine.class);

    public DetectionResult detect(NeuralNetwork nn,
                                  Bucket[] buckets,
                                  DetectionSupplier supplier) {
        List<Double> args = new ArrayList<>();
        List<Double> values = new ArrayList<>();

        double[] output = supplier.outputVector();
        for (int bucketNo = 0; bucketNo < buckets.length; ++bucketNo) {
            double[] test = supplier.inputVector(buckets[bucketNo]);
            nn.predict(test, output);
            if (supplier.isHit()) {
                LOG.info(String.format("%s => %s", Arrays.toString(test), Arrays.toString(output)));
                args.add((double) bucketNo);
                values.add(supplier.detectionValue(buckets[bucketNo], output));
            }
        }
        return new DetectionResult(
                args.stream().mapToDouble(x -> x).toArray(),
                values.stream().mapToDouble(x -> x).toArray()
        );
    }
}
