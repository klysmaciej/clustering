package io.ionate.ml.detection;

import io.ionate.ml.neural.TrainingEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticDetectionTest implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(StaticDetectionTest.class);
    private static final boolean STATIC_TEST_ENABLED = false;

    private static double ANOMALY_PROBABILITY_THRESHOLD = 0.9;
    private final TrainingEngine trainingEngine;

    public StaticDetectionTest(TrainingEngine trainingEngine) {
        this.trainingEngine = trainingEngine;
    }

    @Override
    public void run() {
        while(true) {
            if(STATIC_TEST_ENABLED) {
                this.doTest();
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                LOG.warn("Learning has been interrupted", ex);
                return;
            }
        }
    }

    private void doTest() {
//        double[][] testVectors = new double[][] {
//                {-100}, {-50}, {-10}, {-5}, {-2}, {-1.1}, {-1.01},
//                {-1}, {-0.75}, {-0.5}, {-0.25}, {0}, {0.25}, {0.5}, {0.75}, {1},
//                {1.01}, {1.1}, {2}, {5}, {10}, {50}, {100}
//        };
//
//        for(int i = 0; i < testVectors.length; ++i) {
//            double[] predictedProbability = trainingEngine.predict(testVectors[i]);
//            String verdict = predictedProbability[0] > ANOMALY_PROBABILITY_THRESHOLD ?
//                    "OK" : "WRONG";
//            LOG.info("Value={}, probability={}{{}}", testVectors[i][0], verdict, predictedProbability[0]);
//        }
    }
}
