package io.ionate.ml.detection;

import io.ionate.ml.neural.TrainingEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;

public class DynamicDetectionTest implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicDetectionTest.class);
    private static final boolean DYNAMIC_TEST_ENABLED = false;

    private static double ANOMALY_PROBABILITY_THRESHOLD = 0.9;
    private final TrainingEngine trainingEngine;
    private final Queue<Double> dataSet;

    public DynamicDetectionTest(TrainingEngine trainingEngine, Queue<Double> dataSet) {
        this.trainingEngine = trainingEngine;
        this.dataSet = dataSet;
    }

    @Override
    public void run() {
        while(true) {
            if(DYNAMIC_TEST_ENABLED) {
                if(!dataSet.isEmpty()) {
                    this.doTest();
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                LOG.warn("Learning has been interrupted", ex);
                return;
            }
        }
    }

    private void doTest() {
//        double valueToTest = dataSet.peek();
//        double[] predictedProbability = trainingEngine.predict(new double[] {valueToTest} );
//        String verdict = predictedProbability[0] > ANOMALY_PROBABILITY_THRESHOLD ?
//                "OK" : "WRONG";
//        LOG.info("Value={}, probability={}{{}}", valueToTest, verdict, predictedProbability[0]);
    }
}
