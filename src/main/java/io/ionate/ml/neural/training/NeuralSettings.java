package io.ionate.ml.neural.training;

import java.util.Arrays;

public interface NeuralSettings {

    double getLearningRate();

    int[] getHiddenLayersSize();

    long getEpochs();

    double getStopValue();

    class FixedSettings implements NeuralSettings {
        private final double FIXED_LEARNING_RATE = 0.01;
        private final int[] FIXED_NEURAL_STRUCTURE = new int[]{25, 15};
        private final long FIXED_EPOCH_COUNT = 20000;
        private final double FIXED_STOP_VALUE = 1e-6;

        public double getLearningRate() {
            return FIXED_LEARNING_RATE;
        }

        public int[] getHiddenLayersSize() {
            return FIXED_NEURAL_STRUCTURE;
        }

        public long getEpochs() {
            return FIXED_EPOCH_COUNT;
        }

        public double getStopValue() {
            return FIXED_STOP_VALUE;
        }

        @Override
        public String toString() {
            return "FixedSettings{" +
                    "FIXED_LEARNING_RATE=" + FIXED_LEARNING_RATE +
                    ", FIXED_NEURAL_STRUCTURE=" + Arrays.toString(FIXED_NEURAL_STRUCTURE) +
                    ", FIXED_EPOCH_COUNT=" + FIXED_EPOCH_COUNT +
                    ", FIXED_STOP_VALUE=" + FIXED_STOP_VALUE +
                    '}';
        }
    }

    class CustomSettings implements NeuralSettings {
        private final double learningRate;
        private final int[] hiddenLayersSize;
        private final long epochs;
        private final double stopValue;

        public CustomSettings(double learningRate, int[] hiddenLayersSize, long epochs) {
            this(learningRate, hiddenLayersSize, epochs, 1e-6);
        }

        public CustomSettings(double learningRate, int[] hiddenLayersSize, long epochs, double stopValue) {
            this.learningRate = learningRate;
            this.hiddenLayersSize = hiddenLayersSize;
            this.epochs = epochs;
            this.stopValue = stopValue;
        }

        public double getLearningRate() {
            return learningRate;
        }

        public int[] getHiddenLayersSize() {
            return hiddenLayersSize;
        }

        public long getEpochs() {
            return epochs;
        }

        public double getStopValue() {
            return stopValue;
        }

        @Override
        public String toString() {
            return "CustomSettings{" +
                    "learningRate=" + learningRate +
                    ", hiddenLayersSize=" + Arrays.toString(hiddenLayersSize) +
                    ", epochs=" + epochs +
                    ", stopValue=" + stopValue +
                    '}';
        }
    }
}
