package io.ionate.ml.neural.training;

import io.ionate.ml.data.Guid;
import io.ionate.ml.data.set.Bucket;
import smile.classification.NeuralNetwork;

import java.io.Serializable;

public interface TrainingSettings extends Serializable {

    Guid sourceDataSet();

    TrainingSet trainingSet();

    NeuralNetwork createNeuralNetwork();

    NeuralSettings neuralSettings();

    class TrainingSet {
        private final Bucket[] buckets;
        private final double[][] input;
        private final double[][] output;

        TrainingSet(Bucket[] buckets, double[][] input, double[][] output) {
            this.buckets = buckets;
            this.input = input;
            this.output = output;
        }

        public Bucket[] getBuckets() {
            return buckets;
        }

        public double[][] getInput() {
            return input;
        }

        public double[][] getOutput() {
            return output;
        }
    }
}
