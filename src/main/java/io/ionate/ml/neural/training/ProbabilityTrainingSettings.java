package io.ionate.ml.neural.training;

import io.ionate.ml.data.Guid;
import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.DataSet;
import smile.classification.NeuralNetwork;

/**
 * [1.0, 0.5, 0.0]
 * <p>
 * n     n+1
 * 1.0 -> 1.0 => 1.0 (n+1)
 * 1.0 -> 0.5 => 0.5 (n+1)
 * 1.0 -> 0.0 => 0.0 (n+1)/n
 * <p>
 * 0.5 -> 1.0 => 1.0 (n+1)
 * 0.5 -> 0.5 => 0.5 (n+1)/n
 * 0.5 -> 0.0 => 0.0 (n+1)
 * <p>
 * 0.0 -> 1.0 => 1.0 (n+1)
 * 0.0 -> 0.5 => 0.5 (n+1)
 * 0.0 -> 0.0 => 0.0 (n+1)/n
 */

public class ProbabilityTrainingSettings implements TrainingSettings {

    private static final int INPUT_LAYER_SIZE = 2;
    private static final int OUTPUT_LAYER_SIZE = 1;

    private final Guid sourceDataSet;
    private final Bucket[] trainingBuckets;
    private final NeuralSettings neuralSettings;

    private final double SIGMA_100 = 0.00;
    private final double SIGMA_95 = 0.06;
    private final double SIGMA_90 = 0.13;
    private final double SIGMA_80 = 0.2;
    private final double SIGMA_70 = 0.39;
    private final double SIGMA_60 = 0.53;
    private final double SIGMA_50 = 0.68;
    private final double SIGMA_33 = 1;
    private final double SIGMA_14 = 1.5;
    private final double SIGMA_05 = 2.0;
    private final double SIGMA_01 = 2.5;
    private final double SIGMA_00 = 3.0;

    private final int SIGMA_MUL_INDEX = 0;
    private final int SIGMA_PROBABILITY_INDEX = 1;

    private final double[][] SIGMAS = {
//            { SIGMA_00, 0.00 },
//            { SIGMA_01, 0.01 },
//            { SIGMA_05, 0.05 },
//            { SIGMA_14, 0.14 },
//            { SIGMA_33, 0.33 },
            {SIGMA_33, 0.00},
//            { SIGMA_50, 0.50 },
//            { SIGMA_50, 0.50 },
//            { SIGMA_60, 0.60 },
//            { SIGMA_70, 0.70 },
//            { SIGMA_60, 0.0 },
//            { SIGMA_70, 0.0 },
//            { SIGMA_80, 0.80 },
//            { SIGMA_80, 0.00 },
//            { SIGMA_90, 0.90 },
//            { SIGMA_95, 0.95 },
            {SIGMA_100, 1.00}
    };

    public ProbabilityTrainingSettings(DataSet<Bucket> trainingBucketsDataSet, NeuralSettings neuralSettings) {
        this.sourceDataSet = trainingBucketsDataSet.guid();
        this.trainingBuckets = trainingBucketsDataSet.toArray(new Bucket[0]);
        this.neuralSettings = neuralSettings;
    }

    @Override
    public Guid sourceDataSet() {
        return null;
    }

    @Override
    public TrainingSettings.TrainingSet trainingSet() {
        double[][] input = new double[5 * (trainingBuckets.length - 1)][INPUT_LAYER_SIZE];
        double[][] output = new double[5 * (trainingBuckets.length - 1)][OUTPUT_LAYER_SIZE];

        for (int i = 0; i < trainingBuckets.length - 1; i += 5) {
            // Growing stdDev, mean ~constant
            input[i][0] = trainingBuckets[i].getMean();
            input[i][1] = trainingBuckets[i].getStdDev();
            output[i][0] = 1;

            input[i + 1][0] = trainingBuckets[i].getMean();
            input[i + 1][1] = 5 * trainingBuckets[i].getStdDev();
            output[i + 1][0] = .5;

            input[i + 2][0] = trainingBuckets[i].getMean();
            input[i + 2][1] = 25 * trainingBuckets[i].getStdDev();
            output[i + 2][0] = 0;

            // Growing mean, stdDev ~constant
            input[i + 3][0] = 5 * trainingBuckets[i].getMean();
            input[i + 3][1] = trainingBuckets[i].getStdDev();
            output[i + 3][0] = .5;

            input[i + 4][0] = 25 * trainingBuckets[i].getMean();
            input[i + 4][1] = trainingBuckets[i].getStdDev();
            output[i + 4][0] = 0;
        }

        return new TrainingSettings.TrainingSet(trainingBuckets, input, output);
    }

    @Override
    public NeuralNetwork createNeuralNetwork() {
        int[] numberOfNeuronsInEachLayer = new int[neuralSettings.getHiddenLayersSize().length + 2];
        numberOfNeuronsInEachLayer[0] = INPUT_LAYER_SIZE;
        numberOfNeuronsInEachLayer[numberOfNeuronsInEachLayer.length - 1] = OUTPUT_LAYER_SIZE;
        for (int i = 1; i < numberOfNeuronsInEachLayer.length - 1; ++i) {
            numberOfNeuronsInEachLayer[i] = neuralSettings.getHiddenLayersSize()[i - 1];
        }

        NeuralNetwork nn = new NeuralNetwork(
                NeuralNetwork.ErrorFunction.LEAST_MEAN_SQUARES,
                NeuralNetwork.ActivationFunction.LOGISTIC_SIGMOID,
                numberOfNeuronsInEachLayer);
        nn.setLearningRate(neuralSettings.getLearningRate());

        return nn;
    }

    @Override
    public NeuralSettings neuralSettings() {
        return neuralSettings;
    }
}
