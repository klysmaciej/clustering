package io.ionate.ml.neural;

import io.ionate.ml.data.mapper.Mapper;
import io.ionate.ml.data.mapper.PeriodicSourceMapper;
import io.ionate.ml.data.set.Bucket;
import io.ionate.ml.data.set.BucketAggregator;
import io.ionate.ml.data.set.BucketDataSets;
import io.ionate.ml.data.set.ConstraintHorizonDataSet;
import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.data.set.SingleElementBucketDataSets;
import io.ionate.ml.data.source.PeriodicSources;
import io.ionate.ml.data.source.SineSource;
import io.ionate.ml.data.source.SineWithNoiseSource;
import io.ionate.ml.data.source.SineWithPeaksSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class NeuralServer implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(NeuralServer.class);
    private static final int BUCKET_SIZE = 10;

    public final BucketDataSets AGGREGATED_DATA_SETS;
    public final SingleElementBucketDataSets RAW_DATA_SETS;
    public final PeriodicSources PERIODIC_SOURCES;

    private final List<Mapper<?, ?>> mappers = new ArrayList<>();
    private final ExecutorService sourceThreads = Executors.newCachedThreadPool();
    private final List<Future<?>> started = new ArrayList<>();

    public NeuralServer() {
        this.AGGREGATED_DATA_SETS = BucketDataSets.INSTANCE;
        this.RAW_DATA_SETS = SingleElementBucketDataSets.INSTANCE;
        this.PERIODIC_SOURCES = PeriodicSources.INSTANCE;
        this.setup();
    }

    private void setup() {
        DataSet<Bucket> sourceSignalAggregated = new ConstraintHorizonDataSet<>();
        DataSet<DoubleBucket> sourceSignalDataSet = new BucketAggregator(sourceSignalAggregated, BUCKET_SIZE);
        DataSet<Bucket> realSignalAggregated = new ConstraintHorizonDataSet<>(300);
        DataSet<DoubleBucket> realSignalNotAggregated = new ConstraintHorizonDataSet<>(300 * BUCKET_SIZE);
        DataSet<DoubleBucket> realSignalDataSet = new BucketAggregator(realSignalAggregated, BUCKET_SIZE);

        RAW_DATA_SETS.add(sourceSignalDataSet);
        RAW_DATA_SETS.add(realSignalNotAggregated);
        RAW_DATA_SETS.add(realSignalDataSet);
        AGGREGATED_DATA_SETS.add(sourceSignalAggregated);
        AGGREGATED_DATA_SETS.add(realSignalAggregated);

        Set<DataSet<DoubleBucket>> sineWithNoiseReceiver = new HashSet<DataSet<DoubleBucket>>() {
            {
                add(sourceSignalDataSet);
            }
        };
        SineWithNoiseSource sineWithNoiseSource = new SineWithNoiseSource("Sine with noise");
        Set<DataSet<DoubleBucket>> realSignalReceivers = new HashSet<DataSet<DoubleBucket>>() {
            {
                add(realSignalDataSet);
                add(realSignalNotAggregated);
            }
        };
        SineWithPeaksSource realSignal = new SineWithPeaksSource("'Real' signal with peaks");
        SineSource sine = new SineSource("Sine");

        PERIODIC_SOURCES.add(sine);
        PERIODIC_SOURCES.add(sineWithNoiseSource);
        PERIODIC_SOURCES.add(realSignal);

        mappers.add(new PeriodicSourceMapper(
                sineWithNoiseSource,
                sineWithNoiseReceiver,
                2500
        ));
        mappers.add(new PeriodicSourceMapper(
                realSignal,
                realSignalReceivers,
                2500
        ));
    }

    @Override
    public void run() {
        mappers.stream()
                .map(sourceThreads::submit)
                .forEach(started::add);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        LOG.info("Stopping Neural server...");
        LOG.info("Stopping source threads");
        started.forEach(startedSource -> startedSource.cancel(true));
        LOG.info("Neural server stopped");
    }
}
