package io.ionate.ml.neural;

import io.ionate.ml.neural.training.TrainingSettings;

import java.util.concurrent.Callable;

public class TrainingRunner implements Callable<TrainingEngine.TrainingResult> {

    private final TrainingEngine trainingEngine = new TrainingEngine();
    private final TrainingSettings settings;

    public TrainingRunner(TrainingSettings settings) {
        this.settings = settings;
    }

    @Override
    public TrainingEngine.TrainingResult call() throws Exception {
        return trainingEngine.doTraining(settings);
    }
}
