package io.ionate.ml.neural;

import io.ionate.ml.neural.training.NeuralSettings;
import io.ionate.ml.neural.training.TrainingSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.io.Serializable;

public class TrainingEngine implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingEngine.class);
    private static long LEARNING_COUNTER = 0;

    public TrainingResult doTraining(TrainingSettings settings) {
        TrainingSettings.TrainingSet trainingSet = settings.trainingSet();
        double[][] inputTimeSeries = trainingSet.getInput();
        double[][] expectedOutput = trainingSet.getOutput();

        NeuralNetwork nn = settings.createNeuralNetwork();
        NeuralSettings neuralSettings = settings.neuralSettings();

        double lastError = Double.MAX_VALUE;
        LOG.info("{}. Training with {} vectors", ++LEARNING_COUNTER, inputTimeSeries.length);
        LOG.info("{}. Training settings: {}", LEARNING_COUNTER, settings.neuralSettings());
        LOG.info("{}. Learning started.", LEARNING_COUNTER);
        for (int epoch = 1; epoch <= neuralSettings.getEpochs() && lastError > neuralSettings.getStopValue(); ++epoch) {
            for (int instanceNo = 0; instanceNo < inputTimeSeries.length; ++instanceNo) {
                lastError = nn.learn(inputTimeSeries[instanceNo], expectedOutput[instanceNo], 1.0);
            }
            if (epoch % 100 == 0 || lastError < neuralSettings.getStopValue()) {
                LOG.info("{}. epoch={}, error={}", LEARNING_COUNTER, epoch, lastError);
            }
        }
        LOG.info("Training finished. Error: {}", lastError);
        return new TrainingResult(settings, nn);
    }

    public class TrainingResult implements Serializable {
        private final TrainingSettings settings;
        private final NeuralNetwork nn;

        public TrainingResult(TrainingSettings settings, NeuralNetwork nn) {
            this.settings = settings;
            this.nn = nn;
        }

        public TrainingSettings getSettings() {
            return settings;
        }

        public NeuralNetwork getNn() {
            return nn;
        }
    }
}
