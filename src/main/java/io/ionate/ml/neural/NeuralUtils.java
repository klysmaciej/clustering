package io.ionate.ml.neural;

import java.util.Arrays;

public class NeuralUtils {

    public static double[][] prepareDataToLearn(double[] output) {
        return Arrays.stream(output)
                .mapToObj(sample -> new double[] { sample })
                .toArray(double[][]::new);
    }
}
