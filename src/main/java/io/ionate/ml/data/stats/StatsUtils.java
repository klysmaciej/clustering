package io.ionate.ml.data.stats;

import java.util.Arrays;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntFunction;
import java.util.function.ToDoubleFunction;

public class StatsUtils {

    public static double[] computeProbabilitiesFromHistogram(double[] samples, double[][] histogram) {
        return Arrays.stream(samples)
                .mapToInt(findBinNumber(histogram))
                .mapToObj(findBin(histogram, samples.length))
                .mapToDouble(sampleCount())
                .map(computeProbability(samples.length))
                .toArray();
    }

    private static int[] computeProbabilitiesLabels(double[] samples, double[][] histogram) {
        return Arrays.stream(samples)
                .mapToInt(findBinNumber(histogram))
                .toArray();
    }

    private static int BIN_LOWER_BOUND_INDEX = 0;
    private static int BIN_UPPER_BOUND_INDEX = 1;
    private static int BIN_COUNTER_INDEX = 2;

    private static double IMPOSSIBLE_EVENT_LOWER_BOUND = Double.MIN_VALUE;
    private static double IMPOSSIBLE_EVENT_UPPER_BOUND = Double.MAX_VALUE;
    private static double IMPOSSIBLE_EVENT_COUNTER_VALUE = 0;

    private static IntFunction<double[]> findBin(double[][] histogram, int numberOfSamples) {
        return (binNumber) -> {
            if(binNumber == -1) {
                return new double[] {
                        IMPOSSIBLE_EVENT_LOWER_BOUND,
                        IMPOSSIBLE_EVENT_UPPER_BOUND,
                        IMPOSSIBLE_EVENT_COUNTER_VALUE
                };
            }
            return new double[]{
                    histogram[BIN_LOWER_BOUND_INDEX][binNumber],
                    histogram[BIN_UPPER_BOUND_INDEX][binNumber],
//                    histogram[BIN_COUNTER_INDEX][binNumber]
                    numberOfSamples
            };
        };
    }

    private static DoubleToIntFunction findBinNumber(double[][] histogram) {
        return sample -> {
            int binNumber = 0;
            while(binNumber < histogram[0].length &&
                    (sample < histogram[BIN_LOWER_BOUND_INDEX][binNumber] ||
                            sample > histogram[BIN_UPPER_BOUND_INDEX][binNumber])) {
                ++binNumber;
            }
            if(binNumber >= histogram[0].length) {
                return -1;
            }
            return binNumber;
        };
    }

    private static ToDoubleFunction<double[]> sampleCount() {
        return histogramBin -> histogramBin[BIN_COUNTER_INDEX];
    }

    private static DoubleUnaryOperator computeProbability(int numberOfSamples) {
        return numberOfEvents -> numberOfEvents / numberOfSamples;
    }
}
