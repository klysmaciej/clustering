package io.ionate.ml.data.source;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Stream;

public class SignalFixture {

    public static double[] linspace(double lowerBound, double upperBound, int numberOfSteps) {
        double step = (upperBound - lowerBound) / (numberOfSteps - 1);
        double[] vector = new double[numberOfSteps];
        for (int i = 0; i < numberOfSteps; ++i) {
            vector[i] = lowerBound + i * step;
        }
        return vector;
    }

    public static double[] sine(double[] input) {
        return output().apply(input, Math::sin);
    }

    public static double[] concatVectors(double[] one, double[] two, double[] three) {
        return Stream.concat(
                Stream.concat(Arrays.stream(one).boxed(), Arrays.stream(two).boxed()),
                Arrays.stream(three).boxed())
                .mapToDouble(x -> x)
                .toArray();
    }

    public static double[] valuesBelowLowerBound(double[] input) {
        double lowerBound = Arrays.stream(input).min().getAsDouble();
        return linspace(10 * lowerBound, 0.01 * lowerBound + lowerBound, input.length);
    }

    public static double[] valuesAboveUpperBound(double[] input) {
        double upperBound = Arrays.stream(input).max().getAsDouble();
        return linspace(upperBound + 0.01 * upperBound, 10 * upperBound, input.length);
    }

    public static double[] sineWithNeighbours(double[] input) {
        double lowerBound = Arrays.stream(input).min().getAsDouble();
        double upperBound = Arrays.stream(input).max().getAsDouble();

        double[] valuesBelow = linspace(10 * lowerBound, 0.01 * lowerBound + lowerBound, input.length);
        double[] valuesAbove = linspace(upperBound + 0.01 * upperBound, 10 * upperBound, input.length);

        // Probabilities with labels (not used)
        return Stream.concat(
                Stream.concat(Arrays.stream(valuesBelow).boxed(), Arrays.stream(input).boxed()),
                Arrays.stream(valuesAbove).boxed())
                .mapToDouble(x -> x)
                .toArray();
    }

    private static BiFunction<double[], DoubleUnaryOperator, double[]> output() {
        return (input, f) -> Arrays.stream(input).map(f).toArray();
    }
}
