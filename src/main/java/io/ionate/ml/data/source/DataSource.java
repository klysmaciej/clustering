package io.ionate.ml.data.source;

public interface DataSource<T> extends Iterable<T>, DataSourceInfo {
}
