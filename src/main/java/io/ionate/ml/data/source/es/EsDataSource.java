package io.ionate.ml.data.source.es;

import io.ionate.ml.data.Guid;
import io.ionate.ml.data.source.DataSource;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Supplier;

public class EsDataSource implements DataSource<SearchHit> {

    private final Guid guid = Guid.build();
    private final String name;
    private final Supplier<Client> client;
    private final Supplier<String> accessToken;
    private final SearchRequestBuilder query;

    public EsDataSource(Supplier<Client> client, Supplier<String> accessToken) {
        this.name = getClass().getSimpleName();
        this.client = client;
        this.accessToken = accessToken;
        this.query = pocQuery();
    }

    public EsDataSource(Supplier<Client> client, Supplier<String> accessToken, SearchRequestBuilder esQuery) {
        this.name = getClass().getSimpleName();
        this.client = client;
        this.accessToken = accessToken;
        this.query = esQuery;
    }

    private SearchRequestBuilder pocQuery() {
        RangeQueryBuilder timestampRange = QueryBuilders.rangeQuery("@timestamp")
                .gte(Date.from(Instant.now().minus(12, ChronoUnit.HOURS)).getTime())
                .lte(Date.from(Instant.now()).getTime())
//                        .format("epoch_millis")
                ;

        TimeValue scrollTimeout = new TimeValue(3600000);
        return client.get()
                .filterWithHeader(Collections.singletonMap("Authorization", this.accessToken.get()))
                .prepareSearch("application-logs-*")
                .setTypes("log")
                .setScroll(scrollTimeout)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setPostFilter(timestampRange)
                .setSize(10)
                .setExplain(true);
    }

    @Override
    public Guid guid() {
        return null;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public Iterator<SearchHit> iterator() {
        return new EsIterator(client, query);
    }
}
