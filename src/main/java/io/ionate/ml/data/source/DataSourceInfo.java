package io.ionate.ml.data.source;

import io.ionate.ml.data.Guid;

public interface DataSourceInfo {
    Guid guid();
    String name();
}
