package io.ionate.ml.data.source;

import io.ionate.ml.data.Guid;

import java.util.Iterator;
import java.util.Random;

public class SineWithNoiseSource implements PeriodicSource {

    private final SineSource delegate;
    private final Random noiseGenerator = new Random();

    public SineWithNoiseSource(String name) {
        this.delegate = new SineSource(name);
    }

    @Override
    public String name() {
        return delegate.name();
    }

    @Override
    public Guid guid() {
        return delegate.guid();
    }

    @Override
    public Iterator<Double> iterator() {
        return new PeriodicSourceIterator(this);
    }

    @Override
    public Double at(int index) {
        return delegate.at(index) + noiseGenerator.nextGaussian() % .1;
    }

    @Override
    public int samplesInPeriod() {
        return delegate.samplesInPeriod();
    }
}
