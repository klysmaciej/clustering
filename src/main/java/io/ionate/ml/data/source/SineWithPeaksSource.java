package io.ionate.ml.data.source;

import io.ionate.ml.data.Guid;

import java.util.Iterator;
import java.util.Random;

public class SineWithPeaksSource implements PeriodicSource {

    private final SineSource delegate;
    private final Random random = new Random();

    public SineWithPeaksSource(String name) {
        this.delegate = new SineSource(name);
    }

    @Override
    public String name() {
        return delegate.name();
    }

    @Override
    public Guid guid() {
        return delegate.guid();
    }

    @Override
    public Iterator<Double> iterator() {
        return new PeriodicSourceIterator(this);
    }

    @Override
    public Double at(int index) {
        boolean isPeak = random.nextInt(100) > 98;
        double peakValue = (random.nextDouble() * 2 - 1) * (isPeak ? 1 : 0);
        return delegate.at(index) + peakValue;
    }

    @Override
    public int samplesInPeriod() {
        return delegate.samplesInPeriod();
    }
}
