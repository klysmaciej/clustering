package io.ionate.ml.data.source;

import java.io.Serializable;
import java.util.Iterator;

public interface PeriodicSource extends DataSource<Double>, Serializable {
    Double at(int index);

    int samplesInPeriod();

    class PeriodicSourceIterator implements Iterator<Double> {

        private final PeriodicSource source;
        private int index = 0;

        public PeriodicSourceIterator(PeriodicSource source) {
            this.source = source;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Double next() {
            return source.at((index++) % source.samplesInPeriod());
        }
    }
}
