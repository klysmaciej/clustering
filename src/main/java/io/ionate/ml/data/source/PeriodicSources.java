package io.ionate.ml.data.source;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum PeriodicSources {
    INSTANCE;

    private final List<PeriodicSource> dataSources = new ArrayList<>();

    public void add(PeriodicSource dataSource) {
        this.dataSources.add(dataSource);
    }

    public List<PeriodicSource> getAll() {
        return Collections.unmodifiableList(dataSources);
    }
}
