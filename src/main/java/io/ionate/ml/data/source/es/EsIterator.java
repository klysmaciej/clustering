package io.ionate.ml.data.source.es;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.SearchHit;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Supplier;

class EsIterator implements Iterator<SearchHit> {

    private final Supplier<Client> client;
    private final SearchRequestBuilder query;
    private final TimeValue scrollTimeout = new TimeValue(3600000);
    private String scrollId;
    private SearchHit[] hits;
    private int hitsIndex;

    public EsIterator(Supplier<Client> client, SearchRequestBuilder query) {
        this.client = client;
        this.query = query;
    }

    @Override
    public boolean hasNext() {
        if(Objects.isNull(scrollId)) {
            SearchResponse response = this.query.get();
            this.updateState(response);
        } else if(hitsIndex >= hits.length) {
            SearchResponse response = client.get()
                    .prepareSearchScroll(scrollId)
                    .setScroll(scrollTimeout)
                    .get();
            this.updateState(response);
        }
        return hitsIndex < hits.length;
    }

    private void updateState(SearchResponse response) {
        this.scrollId = response.getScrollId();
        this.hits = response.getHits().getHits();
        this.hitsIndex = 0;
    }

    @Override
    public SearchHit next() {
        if(hitsIndex >= hits.length) {
            throw new NoSuchElementException(
                    String.format("%s does not contain more elements", getClass().getSimpleName()));
        }
        return hits[hitsIndex++];
    }
}
