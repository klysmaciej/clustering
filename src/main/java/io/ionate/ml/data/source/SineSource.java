package io.ionate.ml.data.source;

import io.ionate.ml.data.Guid;

import java.util.Iterator;

public class SineSource implements PeriodicSource {

    private final Guid guid = Guid.build();
    private final String name;

    private static final double SINE_PERIOD = 2 * Math.PI;
    private static final double SINE_SCALE = 1;

    public static final int NUMBER_OF_SAMPLES = 1001;

    private final double[] input = SignalFixture.linspace(0, SINE_PERIOD, NUMBER_OF_SAMPLES);

    public SineSource(String name) {
        this.name = name;
    }

    @Override
    public Guid guid() {
        return guid;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Iterator<Double> iterator() {
        return new PeriodicSourceIterator(this);
    }

    @Override
    public Double at(int index) {
        return SINE_SCALE * Math.sin(input[(index) % this.samplesInPeriod()]);
    }

    @Override
    public int samplesInPeriod() {
        return NUMBER_OF_SAMPLES;
    }
}
