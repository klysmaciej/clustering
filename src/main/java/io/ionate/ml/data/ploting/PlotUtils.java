package io.ionate.ml.data.ploting;

public class PlotUtils {
    public static double[][] prepareDataToPlotLine(double[] xAxis, double[] yAxis) {
        if (xAxis.length != yAxis.length) {
            throw new IllegalArgumentException(
                    String.format("X axis data and y axis data have to be of the same length (%s vs %s)",
                            xAxis.length,
                            yAxis.length)
            );
        }
        double[][] formattedData = new double[xAxis.length][2];
        for (int i = 0; i < xAxis.length; ++i) {
            double[] single = formattedData[i];
            single[0] = xAxis[i];
            single[1] = yAxis[i];
        }
        return formattedData;
    }
}
