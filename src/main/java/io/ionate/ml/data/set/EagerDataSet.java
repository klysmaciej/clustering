package io.ionate.ml.data.set;

import io.ionate.ml.data.Guid;
import io.ionate.ml.data.source.DataSource;

import java.util.ArrayList;

public class EagerDataSet<DS, T extends DataSetItem> extends ArrayList<T> implements DataSet<T> {

    private final Guid guid = Guid.build();
    private final String name;
    private final DataSource<DS> dataSource;
    private final Mapper<DS, T> mapper;

    public EagerDataSet(String name, DataSource<DS> dataSource, Mapper<DS, T> mapper) {
        this.name = name;
        this.dataSource = dataSource;
        this.mapper = mapper;
        this.populate();
    }

    public EagerDataSet(DataSource<DS> dataSource, Mapper<DS, T> mapper) {
        this.name = getClass().getSimpleName();
        this.dataSource = dataSource;
        this.mapper = mapper;
        this.populate();
    }

    private void populate() {
        for (DS item : dataSource) {
            this.add(mapper.map(item));
        }
    }

    @Override
    public Guid guid() {
        return guid;
    }

    @Override
    public String name() {
        return name;
    }
}
