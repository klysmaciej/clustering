package io.ionate.ml.data.set;

import java.util.Objects;

public class SingleElementBucket<T> {
    private final T value;

    public SingleElementBucket(T value) {
        if(Objects.isNull(value)) throw new IllegalArgumentException("Value must be non null");
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
