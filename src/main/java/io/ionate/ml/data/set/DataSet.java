package io.ionate.ml.data.set;

import java.util.Collection;
import java.util.Iterator;

public interface DataSet<T extends DataSetItem> extends Collection<T>, DataSetInfo {

    default void copyToArrayOfVectors(double[][] arrayOfVectors) {
        int index = 0;
        Iterator<T> dataSetIterator = this.iterator();
        while(dataSetIterator.hasNext() && index < arrayOfVectors.length) {
            arrayOfVectors[index] = dataSetIterator.next().toVector();
            ++index;
        }
    }
}
