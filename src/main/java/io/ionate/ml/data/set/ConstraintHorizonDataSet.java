package io.ionate.ml.data.set;

import com.google.common.collect.Iterables;
import io.ionate.ml.data.Guid;

import java.util.ArrayDeque;
import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConstraintHorizonDataSet<T extends DataSetItem> extends ArrayDeque<T> implements DataSet<T> {

    private static final int DEFAULT_HORIZON = 200;

    private final Guid guid = Guid.build();
    private final String name;
    private final int maxNumberOfSamples;

    public ConstraintHorizonDataSet() {
        this(ConstraintHorizonDataSet.class.getSimpleName(), DEFAULT_HORIZON);
    }

    public ConstraintHorizonDataSet(String name) {
        this(name, DEFAULT_HORIZON);
    }

    public ConstraintHorizonDataSet(int maxNumberOfSamples) {
        this(ConstraintHorizonDataSet.class.getSimpleName(), maxNumberOfSamples);
    }

    public ConstraintHorizonDataSet(String name, int maxNumberOfSamples) {
        this.name = name;
        this.maxNumberOfSamples = maxNumberOfSamples;
    }

    public int remainingCapacity() {
        return maxNumberOfSamples - size();
    }

    @Override
    public boolean add(T e) {
        synchronized (this) {
            checkNotNull(e); // check before removing
            if (maxNumberOfSamples == 0) {
                return true;
            }
            if (size() == maxNumberOfSamples) {
                this.remove();
            }
            super.add(e);
            return true;
        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        synchronized (this) {
            return super.toArray(a);
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        int size = collection.size();
        if (size >= maxNumberOfSamples) {
            clear();
            return Iterables.addAll(this, Iterables.skip(collection, size - maxNumberOfSamples));
        }
        return this.addAll(collection);
    }

    @Override
    public Guid guid() {
        return guid;
    }

    @Override
    public String name() {
        return name;
    }
}
