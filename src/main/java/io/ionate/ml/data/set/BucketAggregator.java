package io.ionate.ml.data.set;

import io.ionate.ml.data.Guid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;

import static com.google.common.base.Preconditions.checkNotNull;

public class BucketAggregator extends ArrayDeque<DoubleBucket> implements DataSet<DoubleBucket> {

    private static final Logger LOG = LoggerFactory.getLogger(BucketAggregator.class);

    private final Guid guid = Guid.build();
    private final String name;
    private final int BUCKET_SIZE;
    private final DataSet<Bucket> delegate;

    public BucketAggregator(DataSet<Bucket> delegate, int bucketSize) {
        this(BucketAggregator.class.getSimpleName(), delegate, bucketSize);
    }

    public BucketAggregator(String name, DataSet<Bucket> delegate, int bucketSize) {
        this.name = name;
        this.delegate = delegate;
        this.BUCKET_SIZE = bucketSize;
    }

    @Override
    public Guid guid() {
        return guid;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean add(DoubleBucket e) {
        synchronized (this) {
            checkNotNull(e); // check before removing
            if (BUCKET_SIZE == 0) {
                return true;
            }
            super.add(e);

            if (size() == BUCKET_SIZE) {
                double minValue = minValue();
                double maxValue = maxValue();
                double mean = mean();
                double stdDev = stdDev(mean);

                Bucket bucket = new Bucket(minValue, maxValue, mean, stdDev);
                delegate.add(bucket);
                this.clear();
            }
            return true;
        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        synchronized (this) {
            return super.toArray(a);
        }
    }

    private double minValue() {
        return this.stream().mapToDouble(DoubleBucket::getValue).min().getAsDouble();
    }

    private double maxValue() {
        return this.stream().mapToDouble(DoubleBucket::getValue).max().getAsDouble();
    }

    private double stdDev(double mean) {
        return this.stream()
                .mapToDouble(DoubleBucket::getValue)
                .map(x -> x - mean)
                .map(x -> Math.pow(x, 2))
                .sum() / this.size();
    }

    private double mean() {
        return this.stream().mapToDouble(DoubleBucket::getValue).sum() / this.size();
    }
}
