package io.ionate.ml.data.set;

import io.ionate.ml.data.Guid;

public interface DataSetInfo {
    Guid guid();
    String name();
}
