package io.ionate.ml.data.set;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum BucketDataSets {
    INSTANCE;

    private final List<DataSet<Bucket>> dataSet = new ArrayList<>();

    public void add(DataSet<Bucket> dataSet) {
        this.dataSet.add(dataSet);
    }

    public List<DataSet<Bucket>> getAll() {
        return Collections.unmodifiableList(dataSet);
    }
}
