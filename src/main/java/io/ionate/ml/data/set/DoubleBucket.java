package io.ionate.ml.data.set;

public class DoubleBucket implements DataSetItem {

    private final double value;

    public DoubleBucket(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public double[] toVector() {
        return new double[] { value };
    }
}
