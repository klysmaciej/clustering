package io.ionate.ml.data.set;

public class Bucket implements DataSetItem {

    public static final int NUMBER_OF_FEATURES = 4;

    private final double min;
    private final double max;
    private final double mean;
    private final double stdDev;

    public Bucket(double min, double max, double mean, double stdDev) {
        this.min = min;
        this.max = max;
        this.mean = mean;
        this.stdDev = stdDev;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getMean() {
        return mean;
    }

    public double getStdDev() {
        return stdDev;
    }

    @Override
    public double[] toVector() {
        return new double[] { min, max, mean, stdDev };
    }
}
