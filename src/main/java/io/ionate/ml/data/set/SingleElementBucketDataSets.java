package io.ionate.ml.data.set;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum SingleElementBucketDataSets {
    INSTANCE;

    private final List<DataSet<DoubleBucket>> dataSet = new ArrayList<>();

    public void add(DataSet<DoubleBucket> dataSet) {
        this.dataSet.add(dataSet);
    }

    public List<DataSet<DoubleBucket>> getAll() {
        return Collections.unmodifiableList(dataSet);
    }
}
