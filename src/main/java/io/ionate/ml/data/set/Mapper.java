package io.ionate.ml.data.set;

public interface Mapper <SOURCE, SET> {
    SET map(SOURCE value);

    class DoubleBucketMapper implements Mapper<Double, DoubleBucket> {

        @Override
        public DoubleBucket map(Double value) {
            return new DoubleBucket(value);
        }
    }
}
