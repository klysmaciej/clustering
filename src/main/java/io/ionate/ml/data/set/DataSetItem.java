package io.ionate.ml.data.set;

import java.io.Serializable;

public interface DataSetItem extends Serializable {
    double[] toVector();
}
