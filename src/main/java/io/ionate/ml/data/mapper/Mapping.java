package io.ionate.ml.data.mapper;

import io.ionate.ml.data.set.DoubleBucket;

public interface Mapping<SOURCE, SET> {
    SET map(SOURCE value);

    class DoubleToDoubleBucketMapping implements Mapping<Double, DoubleBucket> {

        @Override
        public DoubleBucket map(Double value) {
            return new DoubleBucket(value);
        }
    }
}
