package io.ionate.ml.data.mapper;

import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.DataSetItem;
import io.ionate.ml.data.source.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public interface Mapper<SOURCE, SET extends DataSetItem> extends Runnable {

    class Default<SOURCE, SET extends DataSetItem> implements Mapper<SOURCE, SET> {
        private static final Logger LOG = LoggerFactory.getLogger(Mapper.class);

        private final DataSource<SOURCE> source;
        private final Collection<DataSet<SET>> sets;
        private final Mapping<SOURCE, SET> mapping;

        public Default(DataSource<SOURCE> source,
                       Collection<DataSet<SET>> sets,
                       Mapping<SOURCE, SET> mapping) {
            this.source = source;
            this.sets = sets;
            this.mapping = mapping;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                for (SOURCE item : source) {
                    sets.forEach(set -> set.add(mapping.map(item)));
                }
            }
        }
    }
}
