package io.ionate.ml.data.mapper;

import io.ionate.ml.data.set.DataSet;
import io.ionate.ml.data.set.DoubleBucket;
import io.ionate.ml.data.source.PeriodicSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class PeriodicSourceMapper implements Mapper<Double, DoubleBucket> {

    private static final Logger LOG = LoggerFactory.getLogger(PeriodicSourceMapper.class);

    private final PeriodicSource source;
    private final Collection<DataSet<DoubleBucket>> sets;
    private final Mapping<Double, DoubleBucket> mapping = new Mapping.DoubleToDoubleBucketMapping();
    private final long sampleRate;

    public PeriodicSourceMapper(PeriodicSource source,
                                Collection<DataSet<DoubleBucket>> sets,
                                int periodInMills) {
        this.source = source;
        this.sets = sets;
        this.sampleRate = periodInMills / source.samplesInPeriod();
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                for (Double item : source) {
                    sets.forEach(set -> set.add(mapping.map(item)));
                    Thread.sleep(sampleRate);
                }
            } catch (InterruptedException ex) {
                LOG.info("Stopping source {}", source.name());
                Thread.currentThread().interrupt();
            }
        }
    }
}
