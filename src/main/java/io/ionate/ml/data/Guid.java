package io.ionate.ml.data;

import java.io.Serializable;
import java.util.UUID;

public final class Guid implements Serializable {

    private final UUID value;

    private Guid(UUID value) {
        this.value = value;
    }

    public UUID getValue() {
        return value;
    }

    public static Guid build() {
        return new Guid(UUID.randomUUID());
    }

    public static Guid fromString(String maybeGuid) {
        return new Guid(UUID.fromString(maybeGuid));
    }

    @Override
    public String toString() {
        return "{guid=" + value +'}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Guid guid = (Guid) o;

        return value.equals(guid.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
