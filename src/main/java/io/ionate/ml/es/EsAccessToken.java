package io.ionate.ml.es;

import org.elasticsearch.common.settings.SecureString;

import java.util.function.Supplier;

import static org.elasticsearch.xpack.security.authc.support.UsernamePasswordToken.basicAuthHeaderValue;

public class EsAccessToken implements Supplier<String> {
    @Override
    public String get() {
        return basicAuthHeaderValue("elastic", new SecureString("changeme".toCharArray()));
    }
}
