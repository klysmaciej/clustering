package io.ionate.ml.es;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.function.Supplier;

public class EsClient implements Supplier<Client> {

    private static final Logger LOG = LoggerFactory.getLogger(EsClient.class);

    private static final String ELASTICSEARCH_IP = "192.168.99.101";
    private static final int ELASTICSEARCH_PORT = 9300;
    private final Client client;

    public EsClient(Supplier<String> accessToken) {
        Settings settings = Settings.builder()
                .put("cluster.name", "elasticsearch-docker")
                .put("xpack.security.user", "elastic:changeme")
                .build();

        try {
            InetSocketTransportAddress esTcpAddress = new InetSocketTransportAddress(
                    InetAddress.getByName(ELASTICSEARCH_IP),
                    ELASTICSEARCH_PORT);

            client = new PreBuiltXPackTransportClient(settings)
                    .addTransportAddress(esTcpAddress)
                    .filterWithHeader(Collections.singletonMap("Authorization", accessToken.get()));
        } catch (UnknownHostException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public Client get() {
        return client;
    }
}
