package io.ionate.ml.sandbox;

import io.ionate.ml.clustering.persistence.model.Tables;
import io.ionate.ml.clustering.persistence.model.tables.Cluster;
import io.ionate.ml.clustering.persistence.model.tables.ClusteringInput;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;

public class Jooq {

    public static void main(String[] args) {
        String userName = "postgres";
        String password = "postgres";
        String url = "jdbc:postgresql://192.168.99.101:5432/ionate-ml";

        try (Connection conn = DriverManager.getConnection(url, userName, password)) {
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

//            create.insert

            create.insertInto(
                    Tables.CLUSTERING_INPUT,
                    Tables.CLUSTERING_INPUT.CONTAINER_NAME,
                    Tables.CLUSTERING_INPUT.LOG_FILENAME)
                    .values("log-generator", "log-generator.log")
            .execute();

            Result<Record> result = create.select().from(Tables.CLUSTERING_INPUT).join(Tables.CLUSTER).on().fetch();
        }

        // For the sake of this tutorial, let's keep exception handling simple
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
