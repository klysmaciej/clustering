package io.ionate.ml.sandbox;

import io.ionate.ml.data.source.SignalFixture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;
import smile.math.Histogram;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static io.ionate.ml.data.source.SignalFixture.linspace;
import static io.ionate.ml.data.stats.StatsUtils.computeProbabilitiesFromHistogram;
import static io.ionate.ml.neural.NeuralUtils.prepareDataToLearn;

public class SimplestCase {

    private static final Logger LOG = LoggerFactory.getLogger(SimplestCase.class);

    private static final double SINE_COSINE_PERIOD = 2 * Math.PI;
    private static final double SINE_COSINE_UPPER_BOUND = 1;
    private static final double SINE_COSINE_LOWER_BOUND = -1;

    private static final int NUMBER_OF_TRAINING_INSTANCES = 1001;
    private static final int NUMBER_OF_TRAINING_LABELS = 21;

    public static void main(String[] args) {
        // Reference signal
        double[] input = linspace(0, 2 * SINE_COSINE_PERIOD, NUMBER_OF_TRAINING_INSTANCES);
        double[] sineWave = SignalFixture.sine(input);

        // Histogram
        double[] breaks = linspace(SINE_COSINE_LOWER_BOUND, SINE_COSINE_UPPER_BOUND, NUMBER_OF_TRAINING_LABELS);
        double[][] outputHistogram = Histogram.histogram(sineWave, breaks);


        // NN learning
        double[] fullInputSpace = SignalFixture.sineWithNeighbours(sineWave);
        double[] probabilities = computeProbabilitiesFromHistogram(fullInputSpace, outputHistogram);

        NeuralNetwork mlp = new NeuralNetwork(NeuralNetwork.ErrorFunction.LEAST_MEAN_SQUARES, NeuralNetwork.ActivationFunction.LOGISTIC_SIGMOID, 1, 10, 5, 1);
        mlp.setLearningRate(0.4);

        // Convert to form accepted by Smile's NN
        double[][] netInput = prepareDataToLearn(fullInputSpace);
        double[][] expectedOutput = prepareDataToLearn(probabilities);

        long epochs = 5000;
        for(int epoch = 1; epoch <= epochs; ++epoch) {
            double lastError = Double.MAX_VALUE;
            for(int instanceNo = 0; instanceNo < fullInputSpace.length; ++instanceNo) {
                lastError = mlp.learn(netInput[instanceNo], expectedOutput[instanceNo], 1.0);
            }

            if(epoch % 100 == 0) {
                LOG.info("Epoch={}, error={}", epoch, lastError);
            }
        }

        // Tests
        double[] predictedProbability = new double[1];
        double[][] testVectors = new double[][] {
                {-100}, {-50}, {-10}, {-5}, {-2}, {-1.1}, {-1.01},
                {-1}, {-0.75}, {-0.5}, {-0.25}, {0}, {0.25}, {0.5}, {0.75}, {1},
                {1.01}, {1.1}, {2}, {5}, {10}, {50}, {100}
        };

        for(int i = 0; i < testVectors.length; ++i) {
            mlp.predict(testVectors[i], predictedProbability);
            LOG.info("Value={}, probability={}", testVectors[i][0], predictedProbability[0]);
        }
    }

    private static void writeObjectToFile(Object object, String filename) {
        try {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(file);
            oos.writeObject(object);
        } catch (IOException ex) {
            LOG.error("Could not write object to file: {}", ex.getMessage());
        }
    }
}
