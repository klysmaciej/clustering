package io.ionate.ml.sandbox;

import smile.plot.Line;
import smile.plot.LinePlot;
import smile.plot.PlotCanvas;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.IntStream;

import static io.ionate.ml.data.ploting.PlotUtils.prepareDataToPlotLine;

public class WhiteNoise {

    private static final double SINE_COSINE_PERIOD = 2 * Math.PI;
    private static final double SINE_COSINE_UPPER_BOUND = 1;
    private static final double SINE_COSINE_LOWER_BOUND = -1;

    private static final int NUMBER_OF_SIGNAL_SAMPLES = 1001;

    public static void main(String[] args) {
        Random noiseGenerator = new Random();
        noiseGenerator.nextGaussian();

        double[] input = IntStream.range(0, NUMBER_OF_SIGNAL_SAMPLES)
                .mapToDouble(n -> n)
                .toArray();
        double[] whiteNoise = IntStream.range(0, 1001)
                .mapToDouble(n -> noiseGenerator.nextGaussian())
                .map(d -> d % .1)
                .toArray();

        double[] sineInput = linspace(0, 2 * SINE_COSINE_PERIOD, NUMBER_OF_SIGNAL_SAMPLES);
        double[] sineOutput = output().apply(sineInput, Math::sin);

        for(int i = 0; i < sineInput.length; ++i) {
            sineOutput[i] += whiteNoise[i];
        }

        PlotCanvas canvas = LinePlot.plot("White noise", prepareDataToPlotLine(sineInput, sineOutput), Line.Style.SOLID, Color.RED);
//        PlotCanvas canvas = smile.plot.Histogram.plot(output, breaks.length);

        JFrame frame = new JFrame("Line Plot");
        frame.setSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add(canvas);
        frame.setVisible(true);
    }

    private static double[] linspace(double lowerBound, double upperBound, int numberOfSteps) {
        double step = (upperBound - lowerBound) / (numberOfSteps - 1);
        double[] vector = new double[numberOfSteps];
        for (int i = 0; i < numberOfSteps; ++i) {
            vector[i] = lowerBound + i * step;
        }
        return vector;
    }

    private static BiFunction<double[], DoubleUnaryOperator, double[]> output() {
        return (input, f) -> Arrays.stream(input).map(f).toArray();
    }
}
