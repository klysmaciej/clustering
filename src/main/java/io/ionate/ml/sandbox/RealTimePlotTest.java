package io.ionate.ml.sandbox;

import io.ionate.ml.data.ploting.PlotUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.math.Math;
import smile.plot.Line;
import smile.plot.LinePlot;
import smile.plot.PlotCanvas;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.concurrent.Executors;

import static io.ionate.ml.data.source.SignalFixture.linspace;

public class RealTimePlotTest {

    private static final Logger LOG = LoggerFactory.getLogger(RealTimePlotTest.class);

    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;
    private static final int NUMBER_OF_SAMPLES = 1001;
    private static final double RESOLUTION = 2 * Math.PI / NUMBER_OF_SAMPLES;

    public static void main(String[] args) throws InterruptedException {
        double start = 0;

        PlotCanvas signalPlot = LinePlot.plot("Sine", signal(start), Line.Style.SOLID, Color.BLUE);

        JPanel mainPanel = new JPanel(new GridLayout(1, 1));
        mainPanel.add(signalPlot);

        JFrame window = new JFrame();
        window.setTitle("RealTime Plot DEMO");
        window.setSize(new Dimension(WIDTH, HEIGHT));
        window.setLocationRelativeTo(null);
        window.getContentPane().add(mainPanel);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setVisible(true);

        Executors.newSingleThreadExecutor().submit(new TestClass(signalPlot));

//        LinePlot linePlot = (LinePlot) signalPlot.getShapes().get(0);
//        try {
//            Field linePlotField = linePlot.getClass().getDeclaredField("line");
//            linePlotField.setAccessible(true);

//            Line line = (Line) linePlotField.get(linePlot);

//            Field pointsField = line.getClass().getDeclaredField("points");
//            pointsField.setAccessible(true);

//            while (true) {
//                for (int i = 0; i < NUMBER_OF_SAMPLES; ++i) {
//                    start = i * RESOLUTION;
//                    double[][] points = signal(start);
//                    double[] lowerBound = Math.colMin(points);
//                    double[] upperBound = Math.colMax(points);
//                    LOG.info(Arrays.toString(lowerBound));
//                    LOG.info(Arrays.toString(upperBound));

//                    pointsField.set(line, points);
//                    signalPlot.extendBound(lowerBound, upperBound);
//                    Thread.sleep(20);
//                }
//            }
//        } catch (NoSuchFieldException | IllegalAccessException ex) {
//            LOG.error(ex.getMessage());
//        }
    }

    private static double[][] signal(double start) {
        double[] arguments = linspace(start, start + 2 * Math.PI, NUMBER_OF_SAMPLES);
        double[] values = Arrays.stream(arguments).map(Math::sin).toArray();
        return PlotUtils.prepareDataToPlotLine(arguments, values);
    }

    private static class TestClass implements Runnable {
        private final PlotCanvas signalPlot;
        private final Line line;
        private final Field pointsField;

        private TestClass(PlotCanvas signalPlot) {
            this.signalPlot = signalPlot;
            this.line = this.line();
            this.pointsField = this.pointsField(this.line);
        }

        private Line line() {
            LinePlot linePlot = (LinePlot) signalPlot.getShapes().get(0);
            try {
                Field linePlotField = linePlot.getClass().getDeclaredField("line");
                linePlotField.setAccessible(true);

                return (Line) linePlotField.get(linePlot);
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                LOG.error(ex.getMessage());
                return null;
            }
        }

        private Field pointsField(Line line) {
            try {
                Field pointsField = line.getClass().getDeclaredField("points");
                pointsField.setAccessible(true);
                return pointsField;
            } catch (NoSuchFieldException ex) {
                LOG.error(ex.getMessage());
                return null;
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    for (int i = 0; i < NUMBER_OF_SAMPLES; ++i) {
                        double start = i * RESOLUTION;
                        double[][] points = signal(start);
                        double[] lowerBound = Math.colMin(points);
                        double[] upperBound = Math.colMax(points);

                        pointsField.set(line, points);
                        signalPlot.revalidate();
                        signalPlot.extendBound(lowerBound, upperBound);
                        Thread.sleep(20);
                    }
                } catch (IllegalAccessException | InterruptedException ex) {
                    LOG.info(ex.getMessage());
                }
            }
        }
    }
}
