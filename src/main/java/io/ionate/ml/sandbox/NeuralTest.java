package io.ionate.ml.sandbox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.util.Arrays;
import java.util.Scanner;

public class NeuralTest {

    private static final Logger LOG = LoggerFactory.getLogger(NeuralTest.class);
    private static long LEARNING_COUNTER = 0;

    public static void main(String[] args) {
        double[][] input = {
                {-10}, {-9}, {-8}, {-7}, {-6},
                {-5}, {-4}, {-3}, {-2}, {-1},
                {0},
                {1}, {2}, {3}, {4}, {5},
                {6}, {7}, {8}, {9}, {10}
        };
        double[][] expectedOutput = {
                {-9}, {-8}, {-7}, {-6}, {-5},
                {-4}, {-3}, {-2}, {-1}, {0},
                {1}, {2}, {3}, {4}, {5},
                {6}, {7}, {8}, {9}, {10},
                {0}
        };

        double minValue = arrayOfVectorsMinValue(expectedOutput);
        double maxValue = arrayOfVectorsMaxValue(expectedOutput);
        double[][] scaledExpectedOutput = scaleToMatchInterval0To1(expectedOutput, minValue, maxValue);

        NeuralNetwork mlp = new NeuralNetwork(
                NeuralNetwork.ErrorFunction.LEAST_MEAN_SQUARES,
                NeuralNetwork.ActivationFunction.LINEAR,
                1, 20, 15, 1);
        mlp.setLearningRate(0.05);

        long epochs = 150000;
        double lastErrorStopValue = 1e-8;
        double lastError = Double.MAX_VALUE;
        LOG.info("{}. Learning started with {} learning vectors", ++LEARNING_COUNTER, input.length);

        for (int epoch = 1; epoch <= epochs && lastError > lastErrorStopValue; ++epoch) {
            for (int instanceNo = 0; instanceNo < input.length; ++instanceNo) {
                lastError = mlp.learn(input[instanceNo], scaledExpectedOutput[instanceNo], 1.0);
            }
            if (epoch % 100 == 0 || lastError < lastErrorStopValue) {
                LOG.info("{}. epoch={}, error={}", LEARNING_COUNTER, epoch, lastError);
            }
        }

        Scanner scanner = new Scanner(System.in);
        double[] test = new double[1];
        double[] output = new double[1];
        do {
            test[0] = scanner.nextDouble();
            mlp.predict(test, output);
            double expectedValue = test[0] + 1;
            double error = Math.abs(expectedValue - output[0]) / expectedValue;
            LOG.info("Predicted output: {}", output[0] * (maxValue - minValue) + minValue);
            LOG.info("Prediction error: {}%", String.format("%.2f", error));
        } while (true);
    }

    private static double[][] scaleToMatchInterval0To1(double[][] expectedOutput, double minValue, double maxValue) {
        return Arrays.stream(expectedOutput)
                .map(vector -> Arrays.stream(vector).map(value -> (value - minValue) / (maxValue - minValue)).toArray())
                .toArray(double[][]::new);
    }

    private static double arrayOfVectorsMinValue(double[][] arrayOfVectors) {
        return Arrays.stream(arrayOfVectors)
                .mapToDouble(vector -> Arrays.stream(vector).min().getAsDouble())
                .min()
                .getAsDouble();
    }

    private static double arrayOfVectorsMaxValue(double[][] arrayOfVectors) {
        return Arrays.stream(arrayOfVectors)
                .mapToDouble(vector -> Arrays.stream(vector).max().getAsDouble())
                .max()
                .getAsDouble();
    }
}
