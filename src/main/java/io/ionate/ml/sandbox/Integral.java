package io.ionate.ml.sandbox;

import io.ionate.ml.data.source.SignalFixture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.DoubleUnaryOperator;

public class Integral {

    private static final Logger logger = LoggerFactory.getLogger(Integral.class);

    public static void main(String[] args) {
        new Integral().run();
    }

    private void run() {
        for(double x = 0.7; x >= 0; x -= 0.01) {
            logger.info("u~{}s : {}", x, erf(x / Math.sqrt(2)));
        }
    }

    private double erf(double x) {
        return 2 * simpson(f(), 0, x) / Math.sqrt(Math.PI);
    }

    private double simpson(DoubleUnaryOperator f, double lowerBound, double upperBound) {
        int numberOfIntervals = 1000000;
        double[] points = SignalFixture.linspace(lowerBound, upperBound, numberOfIntervals);
        double integral = 0;
        for (int i = 1; i < points.length; ++i) {
            integral += f.applyAsDouble(points[i - 1]);
            double h = points[i - 1] + (points[i] - points[i - 1]) / 2;
            integral += 4 * f.applyAsDouble(h);
            integral += f.applyAsDouble(points[i]);
        }
        integral *= (upperBound - lowerBound) / (6 * numberOfIntervals);
        return integral;
    }

    private DoubleUnaryOperator f() {
        return x -> Math.exp(-(x * x));
    }
}
