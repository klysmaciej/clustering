package io.ionate.ml;

import io.ionate.ml.neural.NeuralServer;
import io.ionate.ml.ui.menu.Menus;
import io.ionate.ml.ui.menu.TestMenu;
import io.ionate.ml.ui.menu.TrainMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.classification.NeuralNetwork;

import java.rmi.RemoteException;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Program {

    private static final Logger LOG = LoggerFactory.getLogger(Program.class);

    private final NeuralServer server = new NeuralServer();
    private final Menus menus = new Menus(server);
    private final TrainMenu trainMenu = new TrainMenu(server);
    private final TestMenu testMenu = new TestMenu(server);
    private NeuralNetwork nn;

    void run() {
        Future<?> serverStarted = null;
        try {
            serverStarted = Executors.newSingleThreadExecutor().submit(server);
            this.menu();
        } finally {
            if (Objects.nonNull(serverStarted)) {
                serverStarted.cancel(true);
            }
        }

    }

    private void menu() {
        int option = 0;
        do {
            LOG.info("##### MENU #####");
            LOG.info("0. END");
            LOG.info("1. List periodic sources");
            LOG.info("2. List single element bucket data sets");
            LOG.info("3. List bucket data sets");
            LOG.info("4. Load ANN");
            LOG.info("5. Train ANN");
            if (Objects.nonNull(nn)) {
                LOG.info("6. Test ANN");
            }

            try {
                Scanner scanner = new Scanner(System.in);
                option = scanner.nextInt();

                switch (option) {
                    case 0: {
                    /* END */
                        break;
                    }
                    case 1: {
                        menus.optionOne();
                        break;
                    }
                    case 2: {
                        menus.optionTwo();
                        break;
                    }
                    case 3: {
                        menus.optionThree();
                        break;
                    }
                    case 4: {
                        this.nn = menus.optionFour();
                        break;
                    }
                    case 5: {
                        this.nn = this.trainMenu.run();
                        break;
                    }
                    case 6: {
                        this.testMenu.run(nn);
                        break;
                    }
                    default: {
                        LOG.warn("Invalid option: " + option);
                        break;
                    }
                }
            } catch (Exception ex) {
                LOG.warn(ex.getMessage());
            }
        } while (option != 0);
    }

    public static void main(String[] args) throws RemoteException, InterruptedException {
        new Program().run();
    }
}
