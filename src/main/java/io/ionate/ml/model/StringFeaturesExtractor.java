package io.ionate.ml.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smile.math.Math;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StringFeaturesExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(StringFeaturesExtractor.class);
    private static final int LETTERS_COUNT = 26;
    public static final int DIM = LETTERS_COUNT + 0;

    private static Predicate<String> isNumber() {
        return (word) -> {
            try {
                Float.valueOf(word);
                return true;
            } catch (NumberFormatException ex) {
                return false;
            }
        };
    }

    private static int[] vowels = {97, 101, 105, 111, 117, 121};

    private static IntPredicate isVowel() {
        return (maybeVowel) -> {
            for (int vowel : vowels) {
                if (vowel == maybeVowel) {
                    return true;
                }
            }
            return false;
        };
    }

    private static double vowels(String[] words) {
        return Arrays.stream(words)
                .flatMapToInt(CharSequence::chars)
                .filter(isVowel())
                .count();
    }

    private static double numbers(String[] words) {
        return Arrays.stream(words).filter(isNumber()).count();
    }

    private static int minWordLength(String[] words) {
        return Arrays.stream(words).mapToInt(String::length).min().getAsInt();
    }

    private static int maxWordLength(String[] words) {
        return Arrays.stream(words).mapToInt(String::length).max().getAsInt();
    }

    private static long capitalLetterWordsCount(String[] words) {
        return Arrays.stream(words).filter(isCapitalLetterWord()).count();
    }

    private static long lettersCount(String[] words) {
        return Arrays.stream(words)
                .map(String::toLowerCase)
                .flatMapToInt(String::chars)
                .filter(isLowerCaseLetter())
                .count();
    }

    static double[] toVector(String theString) {
        final String[] words = Arrays.stream(theString.split(" ")).filter(word -> !word.isEmpty()).toArray(String[]::new);
        long capitalLetterWordsCount = capitalLetterWordsCount(words);
        int[] lettersHistogram = lettersHistogram(words);
        int[] capitalLettersHistogram = capitalLettersHistogram(words);

        double[] features = new double[DIM];
        for (int i = 0; i < LETTERS_COUNT; ++i) {
            features[i] = lettersHistogram[i];
//            features[2*i+1] = capitalLettersHistogram[i];
        }

        double max = Arrays.stream(features).max().getAsDouble();
        features = Arrays.stream(features).map(x -> x/max).toArray();

//        features[2 * LETTERS_COUNT] = words.length;
//        features[2 * LETTERS_COUNT + 1] = numbers(words);
//        features[2 * LETTERS_COUNT + 2] = vowels(words);
//        features[2 * LETTERS_COUNT + 3] = capitalLetterWordsCount;
//        features[2 * LETTERS_COUNT + 4] = minWordLength(words);
//        features[2 * LETTERS_COUNT + 5] = maxWordLength(words);
//        features[2 * LETTERS_COUNT + 6] = lettersCount(words);
        return features;
    }

//    static double[] toVector(String theString) {
//        final String[] words = Arrays.stream(theString.split(" ")).filter(word -> !word.isEmpty()).toArray(String[]::new);
//        int[] histogram = histogram(words);
//
//        double[] features = new double[DIM];
//        for (int i = 0; i < LETTERS_COUNT; ++i) {
//            features[i] = histogram[i];
//        }
//        return features;
//    }

    private static Predicate<String> isCapitalLetterWord() {
        return word -> word.charAt(0) >= 65 && word.charAt(0) <= 90;
    }

    private static IntPredicate isLowerCaseLetter() {
        return letter -> letter >= 95 && letter <= 122;
    }

    private static IntPredicate isUpperCaseLetter() {
        return letter -> letter >= 65 && letter <= 90;
    }

    private static int[] lettersHistogram(String[] words) {
        int[] histogram = new int[26];
        Arrays.stream(words)
                .map(String::toLowerCase)
                .flatMapToInt(String::chars)
                .filter(isLowerCaseLetter())
                .forEach(letter -> ++histogram[letter - 97]);
        return histogram;
    }

    private static int[] capitalLettersHistogram(String[] words) {
        int[] histogram = new int[26];
        Arrays.stream(words)
                .flatMapToInt(String::chars)
                .filter(isUpperCaseLetter())
                .forEach(letter -> histogram[letter - 65] = 1);
        return histogram;
    }

    private static Predicate<String> isFullWord() {
        return maybeWord -> maybeWord.chars().filter(isLowerCaseLetter()).count() == maybeWord.length();
    }

    private static int[] histogram(String[] words) {
        int[] histogram = new int[26];
        Arrays.stream(words)
                .map(String::toLowerCase)
                .filter(isFullWord())
                .forEach(word -> {
                    int first = word.charAt(0);
                    int last = word.charAt(word.length() - 1);
                    ++histogram[first - 97];
                    ++histogram[last - 97];
                });
        return histogram;
    }

    private static Stream<String> base(String sentence) {
        return Arrays.stream(sentence.split(" ")).filter(word -> !word.isEmpty());
    }

    public static void main(String[] args) {
        String sentence1 = "57632 [http-nio-8080-exec-8] INFO  i.i.m.l.shop.ProductController  - Creating new product";
        String sentence2 = "62114 [http-nio-8080-exec-7] INFO  i.i.m.l.shop.ProductController  - Updating product 71";
        String sentence3 = "60376 [http-nio-8080-exec-3] INFO  i.i.m.l.shop.ProductController  - Deleting product 76";
        String sentence4 = "55083 [http-nio-8080-exec-2] INFO  i.i.m.l.shop.CartController  - Adding product 18 to cart 6";
        String sentence5 = "52635 [http-nio-8080-exec-8] INFO  i.i.m.l.shop.CartController  - Removing product 3 from cart 73";
        String[] sentences = {sentence1, sentence2, sentence3, sentence4, sentence5};

//        double[] vector3 = toVector(sentence3);
//        double[] vector4 = toVector(sentence4);
//        LOG.info(Arrays.toString(vector3));
//        LOG.info(Arrays.toString(vector4));
//        double distance = Math.squaredDistance(vector3, vector4);
//        LOG.info(String.valueOf(distance));

//        FeatureVectorEncoder encoder = new FeatureVectorEncoder();

//        double[][] distances = new double[sentences.length][sentences.length];
//        for (int i = 0; i < sentences.length; ++i) {
//            for (int j = 0; j < sentences.length; ++j) {
//                double[] a = toVector(sentences[i]);
//                double[] b = toVector(sentences[j]);
//                double distance = Math.squaredDistance(a, b);
//                distances[i][j] = distance;
//            }
//        }

//        for (double[] distanceRow : distances) {
//            LOG.info(Arrays.toString(distanceRow));
//        }
//        for (String sentence : sentences) {
//            double capitalLetterWordsCount = base(sentence).filter(isCapitalLetterWord()).count();
//            int[] lettersHistogram = lettersHistogram(base(sentence).toArray(String[]::new));
//            LOG.info("{}:\t{}, {}", sentence, capitalLetterWordsCount, Arrays.toString(lettersHistogram));
//        }
    }
}
