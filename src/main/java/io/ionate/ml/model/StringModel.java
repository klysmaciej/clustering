package io.ionate.ml.model;

import io.ionate.ml.data.set.DataSetItem;
import io.ionate.ml.data.set.Mapper;
import org.elasticsearch.search.SearchHit;

public class StringModel implements DataSetItem {

    private final String theString;

    public StringModel(String theString) {
        this.theString = theString;
    }

    public String theString() {
        return theString;
    }

    public double[] toVector() {
        return StringFeaturesExtractor.toVector(this.theString);
    }

    public static final class EsMapper implements Mapper<SearchHit, StringModel> {

        @Override
        public StringModel map(SearchHit value) {
            String message = (String) value.getSource().get("message");
            return new StringModel(message);
        }
    }
}
